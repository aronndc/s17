//console.log("Hello World")
/*
	functions
		- are lines/block of codes that tell out device/application to perform certain tasks when called/invoked
		- are mostly created to create complicated tasks to run several lines on code in succession.
		- they are also used to prevent repeating lines/blocks of code that perform the same task/function.

	syntax:
		funtion functionName(){
			code block(statement)
		}
	>> function keyword
		- used to define a javascript functions
	>> functionName
		- function name. Functions are named to be able to use later in the code.
	>> function block ({})
		- the statements which comprise the body of the function. This is where the code to be executed.
*/
function printName(){
	console.log('My name is Aronn. I am a Software Engineer.');
};

// function invocation - it is common to use the term "call a function instead of "Invoke a function".

printName();

function declaredFunction(){
	console.log('This is a defined function for this line.')
};

declaredFunction();
// result: err, because it is not yet defined.

/* Function Declaration vs Epression
	A function can be created through function declaration by using the function keyword and adding a function name.

	Declared function are not executed immediately. They are "save for later use", and will be executed later, when they are invoked (called upon).

*/
function declaredFunction2(){
	console.log("Hi I am from declared function().");
};

declaredFunction2();// declared functions can be hoisted, as long as the function has been defined.

declaredFunction2();
declaredFunction2();
declaredFunction2();

/*
	function expression
	- a function can also be stored in a variable. This is called a function expression.
	- a function expression is an anonymous function assigned to the variable function.

	anonymous function - function without a name.
*/

let variableFunction = function(){
	console.log('I am from variable function.');
};

variableFunction();
/*
	We can also create a function expression of a named function. However, to invoke the function expression, we invoke it by its variable name, not by its functionn name.

	Function expression are always invoke(called) using the variable name.
*/
let funcExpression = function funcName(){
	console.log('Hello from the other side.');
};

funcExpression();

// You can reassign declared function and function expression to new anonymous function

declaredFunction = function(){
	console.log('Updated Declared Function.');
};
declaredFunction();

funcExpression = function(){
	console.log('Updated Function Expression.');
};

funcExpression();

const constantFunction = function(){
	console.log('Initialized with const.');
};

constantFunction();

// constantFunction = function(){
// 	console.log('Cannot be reassigned');
// };

// constantFunction();
// reasssignment with const function expression is not possible.

/*
	Function Scoping

	Sope is the accessibility (visibility) of variables within our program.
		Javascript Variables has 3 types of scope.
		1. local/block scope
		2. global scope
		3. function scope
*/

{
	let localVar = "Tom Cruise";
}
let globalVar = "Mission Impossible Movie";

// console.log(localVar);
// a local variable will be visible only within a function, where it is defined.
console.log(globalVar);
// a global variable has global scope, which means it can be defined anywhere in your JS code.

// Function scope
/*
	JavaScipt has a function scope: Eac function creates a new scope.
	Variables defined inside a function are not accessible (visible) from outside the function.
	Variables declared with var, let and const are quite similar when declared inside a function.
*/
function showNames(){
	// function scope variable 
	var functionVar = 'Jungkook';
	const functionConst = 'BTS';
	let functionLet = 'Kookie';

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};
showNames();


// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

//Need Function
// You can create another function inside a function. This is called a nested function. This nested function, being inside a new function will have access variable, name as they are within the same scope/code block.

function myNewFunction(){
	let name = 'Yor';

	function nestedFunction(){
		let nestedName = 'Brando';
		console.log(nestedName);
	};
	//console.log(nestedName); // result: err, nestedname for function scope
	nestedFunction();
};
myNewFunction();

// FUnction and Global Scoped Variables
// Global Scoped Variables
let globalName = 'Aronn';

function myNewFunction2(){
	let nameInside = 'Pineda';
	// Variables declared globally (outside function) have Global Scope.
	// GLoval variables can be accessed from anywhere in a JavaScript program including from inside a function.
	console.log(globalName);
	console.log(nameInside);
};
myNewFunction2();

/*
	alert()
	syntax:
		alert('message');
*/

alert("Hello World!");

function showSampleAlert(){
	alert('Hello User!');
};

showSampleAlert();

console.log('I will only login on the console when the alert is dismissed.'); 

/*
	prompt()
	syntax:
		prompt('<dialog')
*/

let samplePrompt = prompt('Enter your name:');
console.log('Hello ' + samplePrompt);

let sampleNullPrompt = prompt("Don't input anything");
console.log(sampleNullPrompt);
// if prompt() is cancelled, the result will be: null
// if there is no input in the prompt, the result will be: empty string.

function printWelcomeMessage(){
	let firstName = prompt('Enter your first name: ');
	let lastName = prompt('Enter your last name: ');
	
	console.log('Hello, ' + firstName + ' ' + lastName + '!');
	console.log("Welcome to Gamer's Guild.");
};

printWelcomeMessage();

// Function Naming Convention
	// Function should be definitive of its task. Usually contains a verb.

function getCourses(){
	let courses = ['Progamming 100', 'Science 101', 'Grammar 102', 'Mathematics 103']
	console.log(courses);
};

getCourses();

function get(){
	let name = "Jimin";
	console.log(name);
};

get();

//avoid pointless and inapproriate function names
function foo(){
	console.log(25 % 5);
}

foo();


// name function in smallcaps. Follow camlecase  when naming functions

function displayCarInfo(){
	console.log('Brand: Toyota');
	console.log('Type: Sedan');
	console.log('Price: 1,500,000')
}

displayCarInfo();
















